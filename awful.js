;(function () {
  function Awful(id, value) {
    this.container = window[id];
    this.contentAttribute = (this.container.value === undefined ? 'innerText' : 'value');
    value !== undefined && (this.container[contentAttribute] = value);
  }
  Awful.prototype.set = function(value) {
    this.container[this.contentAttribute] = value;
  };
  Awful.prototype.value = function() {
    return this.container[this.contentAttribute];
  };
  Awful.prototype.toggle = function() {
    this.container.hidden = !this.container.hidden;
  };
  Awful.prototype.on = function(eventName, fn) {
    if(eventName = 'on' + eventName, container.hasOwnProperty(eventName)) {
      container[eventName] = fn;
    } else {
      return false;
    }
  };

  window.Awful = Awful;
}());
